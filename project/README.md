# CPS 474/574 Software/Language-based Security

### Fall 2021

### Dr. Phu Phung

# Team Project

Your team final project will be graded based on:

* 60% of the implementation 

	- Evaluated by the pre-demos, the final presentation and report as well as the source code 

* 20% of the presentation

* 20% of the final report


## Implementation 

You need to push your code/experiment results on your team's Bitbucket repository. You also need to create a README.md file in the home folder to briefly introduce your project, the course and your group. In this README.md file, you need to list how your project can be downloaded, installed, and run your code as well. 


## Presentation

Each team will have a 5-minute slot to present your project results.  
 
You can use the following suggestion to prepare your slides:

* Slide 1:
	The class information, the instructor, the title of your project, and your names.

* From Slide 2:

	* Your project goals (1 slide)

		* why are you interested in the project. What are the objectives?

	* Background (1-2 slides)

		* The overview of the key background for the audience not familiar with your project area

	* Design and Implementation (2-3 slides)

		*How have you archived the goals? Provide some high-level overview of your solutions, and focus on some concrete implementations. Mention your own ideas if applicable.

	* Demo and Results (1-2 slides)

		* Did you reach the goals of the project? What are the findings and results? Share the outcomes of your project to the audience.

## Project Report

Follow the report template here [report-template.md](report-template.md) to write your report. You need to link your report from this README.md page.


# SUBMISSION

You need to submit the following 3 items (in 3 separate files) in Isidore to be graded:

* The final report in PDF (converted from your report.md file)

* The presentation slides in PDF

* The source code in a single compressed file, e.g., code.zip. NOTE: to include a .zip file in Isidore, click the blue button "or select files from workspace or site". 
  

Please note that only one member of your group can submit. 