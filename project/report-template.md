Your report should be well-structured so that the readers can understand the motivation, background, and the contributions of your work. 
You should write your report in markdown using this template. 
Please NOTE that your report submitted on Isidore must be in PDF. 
You can use the suggested outline below to write your report:

# Course and Instructor information

## Project Title

## Team members

    * Member 1, email
    * Member 2, email


## Project Summary 
(Expected length: 0.5 page)

Summary of the problem, motivation, your work, and the results.

## Introduction 
(Expected length: 1-1.5 pages)

In this section, you will introduce to the problem and the motivation of your work. Summarize your work and your results.

## Background 
(Expected length: 1.5-2 pages)

You will present the background of your work, including the problem in detail and summarize how the problem has been solved by others (related work).

## Project Description 
(Expected length: 2-4 pages)

If this is a survey project, you need to describe how your work has been done, including a detailed summary of the work you have surveyed.

If this work has implementation and/or experiments, you need to present your design in one part and your code/experiment structure in the other. Instructions how to run your code/experiments also must be presented.  


## Results 

(Expected length: 1-2 pages)

You will present the results of your work in this part. Clearly specify what are the contributions of your work. In a subsection, you need to clarify how each of you has contributed to the project.

## Project Prototype 

Include the README.md file and the link to the latest commit of your bitbucket repository

## Appendix 

Unlimited appendix pages to show e.g., screenshots of your demo.


